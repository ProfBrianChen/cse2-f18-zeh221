//////////////
//Zezheng Hua
//zeh221@lehigh.edu
//09/25/2018
//use only switch to make this program

import java.util.Scanner;//import a Scanner

public class CrapsSwitch{//common start for a java program
  public static void main (String args[]){
    
    //choose the method that the player want to use
    Scanner scan = new Scanner(System.in);
    String slang = "";//String slanb
    System.out.println("Please choose the method that you want to play: 1:Random 2:Assign");
    int choice = scan.nextInt();
    
    switch(choice){
        
      case 1://when the player chooses random
        int dice1 = (int)(Math.random()*6)+1;//the range of dice 1
        int dice2 = (int)(Math.random()*6)+1;//the range of dice 2
        int diceVal = dice1 + dice2;//the total value of dice1 and dice2
        int diceDifference = dice1 - dice2;//the difference between dice1 and dice2
        
        switch(diceDifference){//specific situations when the difference is 0
          case 0:
            switch(diceVal){
              case 2: slang = "Snake Eyes";
                break;
              case 4: slang = "Hard Four";
                break;
              case 6: slang = "Hard six";
                break;
              case 8: slang = "Hard Eight";
                break;
              case 10: slang = "Hard Ten";
                break;
              case 12: slang = "Boxcars";
                break;
              default: slang = "Invalid message";
                break;
            }
         break;
            
          default://when the difference is not zero
            switch(diceVal){
              case 3: slang = "Ace Deuce";
                break;
              case 4 :slang = "Easy Four";
                break;
              case 5 :slang = "Fever Five";
                break;
              case 6 :slang = "Easy Six";
                break;
              case 7 :slang = "Seven out";
                break;
              case 8 :slang = "Easy Eight";
                break;
              case 9 :slang = "Nine";
                break;
              case 10 :slang = "Easy Ten";
                break;
              case 11 :slang = "Yo-leven";
                break;
              default :slang = "Invalid message";
            }
            break;
        }
        break;//end of case 1
        
      case 2://when the player choose assign
          System.out.println("Please enter the number of the first dice (1 to 6): ");//type the first number
          int diceNum1 = scan.nextInt();
          System.out.println("Please enter the number of the second dice (1 to 6): ");//type the second number
          int diceNum2 = scan.nextInt();
          
          int totalNum = diceNum1 + diceNum2;//define the total
          int diceDifference2 = diceNum1-diceNum2;//define the difference
          
          switch(diceDifference2){
            case 0: 
             switch(totalNum){
              case 2: slang = "Snake Eyes";
                break;
              case 4: slang = "Hard Four";
                break;
              case 6: slang = "Hard six";
                break;
              case 8: slang = "Hard Eight";
                break;
              case 10: slang = "Hard Ten";
                break;
              case 12: slang = "Boxcars";
                break;
              default: slang = "Invalid message";
                break;
              }
            break;
            
           default://when the difference is not zero
            switch(totalNum){
              case 3: slang = "Ace Deuce";
                break;
              case 4 :slang = "Easy Four";
                break;
              case 5 :slang = "Fever Five";
                break;
              case 6 :slang = "Easy Six";
                break;
              case 7 :slang = "Seven out";
                break;
              case 8 :slang = "Easy Eight";
                break;
              case 9 :slang = "Nine";
                break;
              case 10 :slang = "Easy Ten";
                break;
              case 11 :slang = "Yo-leven";
                break;
              default :slang = "Invalid message";
            }
            break;
          }
        break;//end of case2
        
      default: slang = "Invalid message";
        break;
            
          
        }
    System.out.println("The slang of your dices is " + slang);
  }
}