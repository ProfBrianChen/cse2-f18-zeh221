//////////////
//Zezheng Hua
//zeh221@lehigh.edu
//09/25/2018
//use if else to choose the method that the player want to use, and get the outcome
import java.util.Scanner;

//common start for java
public class CrapsIf{
  public static void main(String args[]){
    
    //define the outcome
    String outcome = "";
 
    //Generate random numbers
    int diceVal1 = 0;
    int diceVal2 = 0;
    
   Scanner scan = new Scanner(System.in);
   //ask the players do they want to use random or choose by themselves
   System.out.println("Please enter the number in front of the methods of inputting: 1: Random 2: Assign");
   
   int decision = scan.nextInt();//print in the method that you want to use
   
   if (decision == 1){//if choose the random method
    diceVal1 = (int)(Math.random()*6)+1;
    diceVal2 = (int)(Math.random()*6)+1;
   }
   else if (decision == 2){//if let the computer to assign two values
    System.out.println("Please enter the number of the first dice: ");
    diceVal1 = scan.nextInt();
     if (diceVal1 < 1 || diceVal1 > 6){
       System.out.println("invalid input");
     }
     
    System.out.println("Please enter the number of the second dice: ");
    diceVal2 = scan.nextInt();
     if (diceVal2 < 1 || diceVal2 > 6){
       System.out.println("invalid input");
     }
     
   }
   
   //define the result by the values of 2 dices
    if (diceVal1 ==1 && diceVal2 ==1){
      outcome = "Snake Eyes";
    }
    else if (diceVal1==1 && diceVal2==2){
      outcome = "Ace Deuce";
    }
    else if (diceVal1==1 && diceVal2==3){
      outcome = "Easy Four";
    }
    else if (diceVal1==1 && diceVal2==4){
      outcome = "Fever Five";
    }
    else if (diceVal1==1 && diceVal2==5){
      outcome = "Easy Six";
    }
    else if (diceVal1==1 && diceVal2==6){
      outcome = "Seven out";
    }
    else if (diceVal1==2 && diceVal2==1){
      outcome = "Ace Deuce";
    }
    else if (diceVal1==2 && diceVal2==2){
      outcome = "Hard four";
    }
    else if (diceVal1==2 && diceVal2==3){
      outcome = "Fever five";
    }
    else if (diceVal1==2 && diceVal2==4){
      outcome = "Easy six";
    }
    else if (diceVal1==2 && diceVal2==5){
      outcome = "Seven out";
    }
    else if (diceVal1==2 && diceVal2==6){
      outcome = "Easy Eight";
    }
    else if (diceVal1==3 && diceVal2==1){
      outcome = "Easy Four";
    }
    else if (diceVal1==3 && diceVal2==2){
      outcome = "Fever five";
    }
    else if (diceVal1==3 && diceVal2==3){
      outcome = "Hard six";
    }
    else if (diceVal1==3 && diceVal2==4){
      outcome = "Seven out";
    }
    else if (diceVal1==3 && diceVal2==5){
      outcome = "Easy Eight";
    }
    else if (diceVal1==3 && diceVal2==6){
      outcome = "Nine";
    }
    else if (diceVal1==4 && diceVal2==1){
      outcome = "Fever Five";
    }
    else if (diceVal1==4 && diceVal2==2){
      outcome = "Easy six";
    }
    else if (diceVal1==4 && diceVal2==3){
      outcome = "Seven out";
    }
    else if (diceVal1==4 && diceVal2==4){
      outcome = "Hard Eight";
    }
    else if (diceVal1==4 && diceVal2==5){
      outcome = "Nine";
    }
    else if (diceVal1==4 && diceVal2==6){
      outcome = "Easy Ten";
    }
    else if (diceVal1==5 && diceVal2==1){
      outcome = "Easy Six";
    }
    else if (diceVal1==5 && diceVal2==2){
      outcome = "Seven out";
    }
    else if (diceVal1==5 && diceVal2==3){
      outcome = "Easy Eight";
    }
    else if (diceVal1==5 && diceVal2==4){
      outcome = "Nine";
    }
    else if (diceVal1==5 && diceVal2==5){
      outcome = "Hard Ten";
    }
    else if (diceVal1==5 && diceVal2==6){
      outcome = "Yo-leven";
    }
    else if (diceVal1==6 && diceVal2==1){
      outcome = "Seven out";
    }
    else if (diceVal1==6 && diceVal2==2){
      outcome = "Easy Eight";
    }
    else if (diceVal1==6 && diceVal2==3){
      outcome = "Nine";
    }
    else if (diceVal1==6 && diceVal2==4){
      outcome = "Easy Ten";
    }
    else if (diceVal1==6 && diceVal2==5){
      outcome = "Yo-leven";
    }
    else if (diceVal1==6 && diceVal2==6){
      outcome = "Boxcars";
    }
    
    else if (diceVal1 <= 0 && diceVal1 >= 7 && diceVal2 <= 0 && diceVal2 >= 7){
      outcome = "Invalid";
     }//if the input is not in the range
    
    //print out the outcome 
    System.out.println("The outcome of your dices is " + outcome);
    
  }
}