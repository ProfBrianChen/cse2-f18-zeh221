//////////////
//Zezheng Hua
//zeh221@lehigh.edu
//Card generator
public class CardGenerator{
  //common start for java
  public static void main(String args[]){
    //define the card colors
    String color = "";
    //define card numbers
    String Identity = "";
    
    //Generate random numbers
    int cardNum = (int)(Math.random()*52)+1;
   
    //Now define the color of cards by their numbers
    if (cardNum >= 1 && cardNum <=13){
      color = "diamonds";
    }
    else if (cardNum >=14 && cardNum <=26){
      color = "clubs";
    }
    else if (cardNum >=27 && cardNum <=39){
      color = "hearts";
    }
    else if (cardNum >=40 && cardNum <=52){
      color = "spades";
    }
    
    cardNum = cardNum%13;
    //use modulus to get the remainder
    switch(cardNum){//switch numbers to specific numbers in its color
      case 1: Identity = "Ace";//If the remainder is 1, it is an Ace
        break;//if the remainder is not 1, do the next step
      case 2: Identity = "2";
        break;
      case 3: Identity = "3";
        break;
      case 4: Identity = "4";
        break;
      case 5: Identity = "5";
        break;
      case 6: Identity = "6";
        break;
      case 7: Identity = "7";
        break;
      case 8: Identity = "8";
        break;
      case 9: Identity = "9";
        break;
      case 10: Identity = "10";
        break;
      case 11: Identity = "Jack";
        break;
      case 12: Identity = "Queen";
        break;
      case 0: Identity = "King";
        break;
      default: Identity = "Invalid messages";
        break;//type this, so that when error happens, I can notice it
    }
    //print out the result
    System.out.println("You picked the " + Identity + " of " + color);
  }
}