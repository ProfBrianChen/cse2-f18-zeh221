import java.util.Arrays;
public class SelectionSortLab10 {
  public static void main (String [] args){
    int [] myArrayBest = {1,2,3,4,5,6,7,8,9};
    int [] myArrayWorst = {9,8,7,6,5,4,3,2,1};
    int iterBest = selectionSort (myArrayBest);
    int iterWorst = selectionSort(myArrayWorst);
    System.out.println("The total number of operations performed on the sorted array: " + iterBest);
    System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
  }
  
  public static int selectionSort (int[] list){
    System.out.println(); //prints the initial array (insert another)
    //print out statement later in the code to show the array as it's being sorted
    System.out.println(Arrays.toString(list));
    //initialize counter for interations
    int iteration = 0;
    
    for (int i = 0; i < list.length - 1; i++) {
    iteration++;// Update the iterations counter
      
   
   // Step One: Find the minimum in the list[i..list.length-1]
   int currentMin = list[i]; 
   int currentMinIndex = i;
   for (int j = i + 1; j < list.length; j++) { 
     if(list[j]<list[i]){
     i=j;
     iteration++;
     }// COMPLETE THIS FOR LOOP
    // (In this step, make sure you update the iteration 
    // counter time you compare the current min to another 
    // element in the array.)
   }

   // Step Two: Swap list[i] with the minimum you found above
   if (currentMinIndex != i) { 
   int temp = list[i];
   list [i] = list [currentMinIndex];
   list [currentMinIndex] = temp;
   // COMPLETE THE CODE TO MAKE THE SWAP 
   }
  }
  return iteration;
 

  }
}