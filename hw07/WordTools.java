import java.util.Scanner;//going to use scanner

public class WordTools{//starting a program
  public static void main(String args[]){
  Scanner scan = new Scanner(System.in);//define it as scan
    
//define variables
  char option = ' ';//define a char
  String text = "";//the text that you are going to enter
  String findText = "";
  
  text = sampleText();//define text
  
  System.out.println("You entered: " + text);//get the result
  
  //quit
  while(option != 'q'){
  //set up the menu
    option = printMenu();
    
    //if c is pressed
    if(option == 'c'){
      System.out.println("Number of non-whitespace characters: " + getNumOfNonWSCharacters(text));
    }
    
    //if w is pressed
    else if(option == 'w'){
      System.out.println("Number of non-whitespace characters: " + getNumOfWords(text));
    }
    
    //if f is pressed
    else if(option == 'f'){
      System.out.println("Enter a word to be found: ");
      findText = scan.nextLine();
      System.out.println("\"" + findText + "\" instances: " + findText(findText,text));
    }
    
    //if r is pressed
    else if(option == 'r'){
      System.out.println("Edited text: " + replaceExclamation(text));
    }
    
    //if s is pressed
    else if(option == 's'){
      System.out.println("Edited text: " + shortenSpace(text));
    }
  }
  }
  
  public static char printMenu(){
  char input = ' ';
  char junk = ' ';
  Scanner scan = new Scanner(System.in);
  
  //print out the menu
  System.out.println("Menu");
  System.out.println("c - Number of non-whitespace characters");
  System.out.println("w - Number of words");
  System.out.println("f - Find text");
  System.out.println("r - Replace all !'s");
  System.out.println("s - Shorten spaces");
  System.out.println("q - Quit");
  System.out.println();
  System.out.println("Choose an option: ");
  //option
  input = scan.next().charAt(0);
  //show up the menu again if the input is invalid
  while(input != 'c' && input != 'w' && input != 'f' && input != 'r' && input != 's' && input != 'q'){
    System.out.println("Choice again");
  input = scan.next().charAt(0);
  }
  return input;
  
  }
  
  public static String sampleText(){//sample text inputting
    Scanner scan = new Scanner(System.in);
    String outcome = "";
    
    System.out.println("Enter a text: ");
    outcome = (scan.nextLine());
    
    return outcome;
  }
  
  public static String getNumOfNonWSCharacters(String a){//get number of non whitespace characters
  String outcome = "";
  int character = 0;
  int i = 0;
  //checking process
  for(i =0; i <a.length();i++){
    if(a.charAt(i) != ' '){
    character++;
    }
  }
  outcome = String.valueOf(character);//convert from int to string
  return outcome;
  }
  
  public static String getNumOfWords(String a){//getting number of words
    String outcome = "";
    int word = 0;
    int i = 0;
    String line = shortenSpace(a);
    //checking
    for(i = 0;i <line.length();i++){//count everyspace after each word
      if(line.charAt(i) == ' '){
      word++;
      }
    }
    
    outcome = String.valueOf(word + 1);//add the last word
    return outcome;
      
  }
  
  public static String findText(String a, String b){//find text
    String outcome = "";
    int check = 0;
    int i = 0;
    int j = 0;
    
    for(i = 0;i < b.length();i++){
      //when find the same character
      if(b.charAt(i) == a.charAt(j)){
      j++;//j plus one to check the next character
      if(j == a.length()){
        check++;
        j = 0;//reset j
      }
      }
    }
    outcome = String.valueOf(check);//convert outcome to String
    return outcome;
      
  }
  
  public static String replaceExclamation(String a){//replace ! with 
    return a.replaceAll("!",",");
  
  }
  
  public static String shortenSpace(String a){//replace double space with single space
    String space = " ";
    String doubleSpace = "  ";
    
    return a.replaceAll(doubleSpace,space);
  }
 
  
  
}//end of a program