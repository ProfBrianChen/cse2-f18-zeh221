import java.util.Random;//going to use random later

public class Arrays{//common start
  public static void main(String args[]){
  //declare arrays and variables
    int array1[] = new int[100];//with a size of 100
    int array2[] = new int[100];//also with a size of 100
    int count = 0;
    int i = 0;
    
    Random random = new Random();
    //create the first array
    for(i=0;i<array1.length;i++){
      array1[i]=random.nextInt(100);
    }
    for(i=0;i<array1.length;i++){
      count = array1[i];
      array2[count]++;
    }//second array
    //print out the result
    for(i=1;i<array2.length;i++){
      if(array2[i]>=1){
      System.out.println(i+" occurs "+array2[i]+" times");
      }
    }
  }
}
