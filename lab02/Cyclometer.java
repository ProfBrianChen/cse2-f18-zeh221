//Zezheng Hua
//CSE 002
//09/07/2018
//zeh221@lehigh.edu
// Cyclometer is used to measure and record the speed of a bicycle, and the distance it traveled. 
// In this lab, we want to know what is the distance that the bike traveled, and its speed by using minutes and counts
public class Cyclometer {
  //main method required for every java program
  public static void main(String[] args) {
   
    int secsTrip1=480;  //The first trip is 480 
    int secsTrip2=3220;  //The second trip is 3220 
		int countsTrip1=1561;  // The count of first trip's rotations is 1561
		int countsTrip2=9037; // The count of second trip'a rotations is 9037
    
    double wheelDiameter=27.0;  // The diameter of the wheel is 27.0
  	double PI=3.14159; // The constant that is used to calculate the perimeter
  	double feetPerMile=5280;  // A mile is 5280 feet
  	double inchesPerFoot=12;   // A foot is 12 inches
  	double secondsPerMinute=60;  // A minute is 60 seconds
	  double distanceTrip1, distanceTrip2,totalDistance;  // Distance for trip 1 trip 2 and the total distanceTrip
    
    System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+" minutes and had "+ countsTrip1+" counts."); // Print out for trip 1
	  System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute)+" minutes and had "+ countsTrip2+" counts."); // Print out for trip 2
    
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    	//Above gives distance in inches
    	//for each count, a rotation of the wheel travels
    	//the diameter in inches times PI
	  distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
 	  distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //distance in miles
	  totalDistance=distanceTrip1+distanceTrip2; // distance in miles
    
    //Print out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles"); //conclusion
	  System.out.println("Trip 2 was "+distanceTrip2+" miles"); //conslusion
	  System.out.println("The total distance was "+totalDistance+" miles"); //conclusion


    

    
    
  } //end of main method
} //end of class