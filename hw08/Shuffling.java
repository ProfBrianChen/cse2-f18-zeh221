//////////////
//Zezheng Hua
import java.util.Scanner;//going to use scanner
import java.util.Random;//going to use random
public class Shuffling{ 
public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
 String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52]; 
String[] hand = new String[5]; 
int numCards = 5; 
int again = 1; 
int index = 51;
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  System.out.print(cards[i]+" "); 
} 
System.out.println();
printArray(cards); 
shuffle(cards); 
printArray(cards); 
while(again == 1){ 
   hand = getHand(cards,index,numCards); 
   printArray(hand);
   index = index-numCards;
   System.out.println("Enter a 1 if you want another hand drawn"); 
   again = scan.nextInt(); 
}  
  }

public static void shuffle(String[] cards){

  Random ran = new Random();//generate a random number
  
  for (int i = 0; i < 52; i++){
  int randomNum = ran.nextInt(cards.length);
  String a = cards[0];
  cards[0] = cards[randomNum];
  cards[randomNum]=a;
  
  }
}

public static void printArray(String [] cards){
  for (int i = 0; i < cards.length; i++){
  System.out.print(cards[i]+" ");//print out array
  }
  System.out.println();//change a lane
}

public static String[]getHand(String[] lists, int index, int num){
  String[]hand = new String[num];
  String[]suitNames = {"C","H","S","D"};
  String[]rankNames = {"2","3","4","5","6","7","8","9","10","J","Q","K","A"};
  String[]cards = new String[52];
  if(num > index +1){//new deck
    for (int j = 0; j < 52; j++){
      cards[j]=rankNames[j%13]+suitNames[j/13];
    }
    shuffle(cards);
    index = 51;
  }
  
  for(int i = num - 1; i >= 0; i--){//card that is taken
    hand[i] = lists[index - 4 + i];
  }
  return hand;
}
}//end of the program
