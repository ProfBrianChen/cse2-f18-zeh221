
import java.util.*;
public class CSE2Linear{//common start for java
  public static void main(String args[]){
    Scanner scan = new Scanner(System.in); 
    //variables
    boolean number = true; 
    boolean firstTime = true; 
    int userInput = 0;
    int index = 0; 
    
    int arrayLength = 15;
    int [] grades = new int [arrayLength];     
      while(number)
      {
        System.out.println("Enter 15 ascending ints for final grades of CSE2: ");
        if(!scan.hasNextInt())
        {
          number = true; 
          scan.next(); 
          System.out.println("Error, Please enter an integer: ");//first message
        }
        else if((userInput = scan.nextInt()) < 0 || userInput > 100)
        {
          number = true; 
          System.out.println("This integer is out of range: enter from 0-100"); //second message
        }
        else if(index > 0)
        {
          if(userInput < grades[index - 1])
          {
            number = true; 
            System.out.println("This integer should be larger than the last");//third message
          }
          else
          {
            grades[index] = userInput; 
            index++; 
            if(index == arrayLength)
            {
              number = false; 
            }
          }
        }
        else if(firstTime)
        {
          grades[index] = userInput; 
          index++; 
          firstTime = false; 
        }
      }//end of determining the user input. 
    
    for(int i = 0; i < arrayLength; i++) //print out the ascending array 
    {
      System.out.print(grades[i] + " "); 
    }
    System.out.println(); 
    number = true; 
    
    //check the search input 
    int checkGrade = 0; 
    while(number) 
    {
      System.out.print("Enter a grade for searching: ");
      if(!scan.hasNextInt())
      {
        number = true;
        String trash = scan.next();
        System.out.println("This is not an integer.");
      }
      else
      {
        checkGrade = scan.nextInt();
        number = false;
      }
    }
    binarySearch(grades, checkGrade);//figure binary search
    
    int[] newArray = scrambling(grades);
    System.out.println("Scrambled: ");
        for(int i = 0; i < arrayLength; i++) 
        {
          System.out.print(newArray[i] + " ");
        }
        System.out.println();
    
    //seach again with linear search
    number = true; 
    while(number) 
    {
      System.out.print("Enter a grade for searching: ");
      if(!scan.hasNextInt())
      {
        number = true;
        String trash = scan.next();
        System.out.println("This is not an integer.");
      }
      else
      {
        checkGrade = scan.nextInt();
        number = false;
      }
    }
    linearSearch(newArray,checkGrade); 
    
    
    

   
  }//end of main method
  public static void binarySearch(int[] a, int target)
  {
    int counter = 0; 
    int high = a.length - 1;
    int low = 0;
    int iterations = 0;
    boolean determine = false;
    while (high >= low) 
    {
      int mid = (high + low) / 2;
      if (target > a[mid]) 
      {
        low = mid + 1;
      }
      else if (target < a[mid]) 
      {
        high = mid - 1;
      }
      else if (target == a[mid])
      {               
        iterations++;
        determine = true; 
        break; 
      } 
      iterations++; 
    }
      if(determine) 
      {
        iterations++; 
        System.out.println(target + " was found with " + iterations + " iterations");
      }
      else
      {
        System.out.println(target + " was not found with " + iterations + " iterations");
      }
  }
  public static int[] scrambling(int[] a) //Scramble the array by give them the random position
  {
        int arrayLength = a.length;
        int[] newArray = new int[arrayLength];
        for(int i = 0; i < arrayLength; i++) 
        {
          boolean notFinish = true;
          while(notFinish) 
          {
            int position = (int)(Math.random() * arrayLength);
            if(a[position] != 0) 
            {
              newArray[i] = a[position];
              a[position] = 0; 
              notFinish = false; 
            }
          }
        }
    return newArray;
  }
  public static void linearSearch(int[] a, int target) 
  {
    int i = 0;
    boolean determine = false;
    int iterations = 0;
    for(i = 0; i < a.length; i++) 
    {
      if(a[i] == target) 
      {
        iterations = i; 
        determine = true; 
        break;
      }
    }

    if(determine) 
    {
      System.out.println(target + " was found with " + iterations + " iterations");
    }
    else 
    {
      System.out.println(target + " was not found with 15 iterations");
    }
   }
}//end of program


