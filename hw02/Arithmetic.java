 //////////////
//Zezheng Hua
//zeh221@lehigh.edu
//CSE-002
//Lehigh University
//Professor Arielle Carr
//09/11/2018

import java.text.DecimalFormat;

public class Arithmetic{
  
  public static void main(String args[]){
    
   DecimalFormat df = new DecimalFormat("#.00");
 
    
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    
    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;
    
    //the tax rate
    double paSalesTax = 0.06;
    
    //-----------------------------------------------------------------------
    
    //Calculating the total cost of each products
    double totalCostOfPants = numPants * pantsPrice;
    
    double totalCostOfShirts = numShirts * shirtPrice;
    
    double totalCostOfBelts = numBelts * beltCost;
    
    //Display the total cost of each products
    System.out.println("The total cost of pants is " + totalCostOfPants);
    
    System.out.println("The total cost of sweatshirts is " + totalCostOfShirts);
    
    System.out.println("The total cost of belts is " + totalCostOfBelts);
    
    //Calculating the total cost of all three products
    double totalCost = totalCostOfPants + totalCostOfShirts +totalCostOfBelts;
    
    //Display the total cost of all three products
    System.out.println("The total cost is " + totalCost);
    
    //Calculating the tax charged on each product
    double taxOnPants = totalCostOfPants * paSalesTax;
    
    double taxOnShirts = totalCostOfShirts * paSalesTax;
    
    double taxOnBelts = totalCostOfBelts * paSalesTax;
    
    //Display the tax charged on each product in two decimals
    System.out.println("The tax on pants is " + df.format(taxOnPants));
    
    System.out.println("The tax on sweatshirts is " + df.format(taxOnShirts));
    
    System.out.println("The tax on belts is " + df.format(taxOnBelts));
    
    //Calculating the total cost
    double totalTax = taxOnPants + taxOnShirts + taxOnBelts;
    
    //Display the total tax 
    System.out.println("The total tax is " + df.format(totalTax));
    
    //Calculating the total payment
    double totalPaid = totalCost + totalTax;
    
    //Display the total payment
    System.out.println("The total payment is " + df.format(totalPaid));

    


  }
}