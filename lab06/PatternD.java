import java.util.Scanner;//going to use scanning

public class PatternD{
  public static void main(String args[]){
    
    Scanner scan = new Scanner(System.in);
    
    int num = 0;//number of the integer
    int kk;
    int mm;
      
    System.out.println("Please enter an integer from 1 to 10: ");
    
    boolean isNum = false;//set boolean
    
    while(isNum == false){
      isNum = scan.hasNextInt();
      //now put if else
      if (isNum == true){
        num = scan.nextInt();
        System.out.println("The number you choose is: "+ num);
        
      }//end of if
      else {
        System.out.println("Invalid, please enter an integer between 1 to 10");
        isNum=false;
        scan.next();
      }//end of else
    }//end of while loops
    
    //for loop
    for (kk =1;kk<=num;kk++){
      for (mm=num+1-kk; mm>=1;mm--){
         System.out.print(mm+" ");
 }
     System.out.println();
  }
    
    
    
    
  }
}