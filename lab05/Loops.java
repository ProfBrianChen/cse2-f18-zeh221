//////////////
//Zezheng Hua
//zeh221@lehigh.edu
//In this program, i am going to use loops to get the information that i want

import java.util.Scanner;//I am going to use scanner in this program

public class Loops{
  //common start for java program
  public static void main(String args[]){
    
    Scanner scan = new Scanner(System.in);
    
    //define all the variables
    int courseNum;//course number
    int numOfStudents;//the number of students in the class
    int numOfTimes;//number of times you need to meet in a week
    String classStartsTime;//the time that the class starts
    String departmentName;//name of the departmentName
    String nameOfInstructor;//the name of the instructor
    
    //type the course number
    System.out.println("What is the course number: ");
    
    boolean isNum = false;//set boolean
    
    while(isNum == false){
      isNum = scan.hasNextInt();
      //now put if else
      if (isNum == true){
        courseNum = scan.nextInt();
        System.out.println("Course Number is " + courseNum);
        
      }//end of if
      else {
        System.out.println("Invalid, please enter again");
        isNum=false;
        scan.next();
      }//end of else
    }//end of while loops
    
   
    
    
    boolean isDep = false;//for department name
    System.out.println("What is the department name: ");
    while(isDep == false){
      isDep =scan.hasNext();
      if(isDep == true){
        departmentName=scan.next();
        System.out.println("The department is " + departmentName);
      }
      else {
        System.out.println("Invalid, please enter again");
        isDep=false;
        scan.next();
      }//end of else
    }//end of while loop
    
   
    
    
    boolean isName = false;//for your instructor's name
    System.out.println("What is the name of your instructor: ");
    while(isName== false){
      isName =scan.hasNext();
      if(isName == true){
        nameOfInstructor = scan.next();
        System.out.println("The name of the instructor is  " + nameOfInstructor);
      }
      else {
        System.out.println("Invalid, please enter again");
        isName=false;
        scan.next();
      }//end of else
    }//end of while loop
    
   
    
    
    boolean isNumstu = false;//for number of students
    System.out.println("What is the number of students: ");
    while(isNumstu == false){
      isNumstu =scan.hasNextInt();
      if(isNumstu == true){
        numOfStudents=scan.nextInt();
        System.out.println("The number of students is " + numOfStudents);
      }
      else {
        System.out.println("Invalid, please enter again");
        isNumstu=false;
        scan.next();
      }//end of else
    }//end of while loop
    
    
    
    
    boolean isTime = false;//number of times
    System.out.println("What is the number of times: ");
    while(isTime == false){
      isTime =scan.hasNextInt();
      if(isTime == true){
        numOfTimes=scan.nextInt();
        System.out.println("The number of times is " + numOfTimes);
      }
      else {
        System.out.println("Invalid, please enter again");
        isTime=false;
        scan.next();
      }//end of else
    }//end of while loop
    
    
    
    boolean isStarts = false;//the starting time
    System.out.println("What is the starting time: ");
    while(isStarts == false){
      isStarts =scan.hasNext();
      if(isStarts == true){
        classStartsTime=scan.next();
        System.out.println("The starting time is " + classStartsTime);
      }
      else {
        System.out.println("Invalid, please enter again");
        isStarts=false;
        scan.next();
      }//end of else
    }//end of while loop
    
    
    
    
    
    
    
    
    
  }
}//end of the program



