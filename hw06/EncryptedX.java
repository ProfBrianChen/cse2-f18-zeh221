//////////////
//Zezheng Hua
//zeh221
//the purpose of this lab is to get used to for loop

import java.util.Scanner;//going to use scanning

public class EncryptedX{
  public static void main(String args[]){
    
    Scanner scan = new Scanner(System.in);
    
    int num = 0;//number of the integer
    int row;//the row
    int line;//the number within the row
      
    System.out.println("Please enter an integer from 1 to 100: ");
    
    boolean isNum = false;//set boolean
    
    while(isNum == false){
      isNum = scan.hasNextInt();
      //now put if else
      if (isNum == true){
        num = scan.nextInt();
        System.out.println("The integer is: "+ num);
        
      }//end of if
      else {
        System.out.println("Invalid, please enter an integer between 1 to 100");
        isNum=false;
        scan.next();
      }//end of else
    }//end of while loops
    
    //controling the row
    for(row = 1; row <= num+1; row++){
      //controling the line
      for(line = 0; line <= num; line++){
      //print the first space
        if(line == row -1){
        System.out.print(" ");
        }
        //print the last space
        else if(line == num - row + 1){
        System.out.print(" ");
        }
        //for the else space
        else{
        System.out.print("*");
        }
      }
        System.out.println();//change a line
    }
    
    
  }
}//end of the method