//////////////
//Zezheng Hua

import java.util.Random;
import java.util.Scanner;

public class Methods{
  public static void main(String args[]){
    
    int answer;
    int determinant = 0;
    int counts;
    int randomNum;
    String results = "";
    
    Random random = new Random();
    
    Scanner scan = new Scanner(System.in);
    
    do{
    
    results = "A " + Adjective(random.nextInt(10)) + " " + Objective(random.nextInt(10)) + " " + pastVerb(random.nextInt(10)) + " a " + Adjective(random.nextInt(10))+ " " + Objective(random.nextInt(10)) + " with the " + subNoun(random.nextInt(10))+ ".";
    
     System.out.println(results);
    
      System.out.println("Do you like this sentence? ");
      System.out.println("1.Yes. ");
      System.out.println("2.No. ");
      answer = scan.nextInt();
      if(answer == 1){
        determinant = 1;
      }
      else{
        determinant = 0;
      }
    
    }while (determinant != 1);
    
    int num = random.nextInt(10);
    
    System.out.println(thesis(num));
    System.out.println(paragraph(num));

  }
    public static String Adjective(int a){
      String adjective = "";
      switch(a){
        case 0: adjective = "creepy";
        break;
        case 1: adjective = "interesting";
        break;
        case 2: adjective = "disgusting";
        break;
        case 3: adjective = "horrible";
        break;
        case 4: adjective = "stupid";
        break;
        case 5: adjective = "angry";
        break;
        case 6: adjective = "bold";
        break;
        case 7: adjective = "disappointing";
        break;  
        case 8: adjective = "ugly";
        break;
        case 9: adjective = "pretty";
        break;
      }
      return adjective;
    }


 public static String Objective(int a){
      String objNoun = "";
      switch(a){
        case 0: objNoun = "jason";
        break;
        case 1: objNoun = "sella";
        break;
        case 2: objNoun = "amy";
        break;
        case 3: objNoun = "mike";
        break;
        case 4: objNoun = "colin";
        break;
        case 5: objNoun = "walt";
        break;
        case 6: objNoun = "nick";
        break;
        case 7: objNoun = "allen";
        break;  
        case 8: objNoun = "jack";
        break;
        case 9: objNoun = "leo";
        break;
      }
      return objNoun;
    }


 public static String subNoun(int a){
      String subNoun = "";
      switch(a){
        case 0: subNoun = "bowl";
        break;
        case 1: subNoun = "stone";
        break;
        case 2: subNoun = "sound";
        break;
        case 3: subNoun = "knife";
        break;
        case 4: subNoun = "body";
        break;
        case 5: subNoun = "missile";
        break;
        case 6: subNoun = "dagger";
        break;
        case 7: subNoun = "arrow";
        break;  
        case 8: subNoun = "gun";
        break;
        case 9: subNoun = "sword";
        break;
      }
      return subNoun;
    }


public static String pastVerb(int a){
      String verb = "";
      switch(a){
        case 0: verb = "kicked";
        break;
        case 1: verb = "attacked";
        break;
        case 2: verb = "defended";
        break;
        case 3: verb = "saved";
        break;
        case 4: verb = "killed";
        break;
        case 5: verb = "detected";
        break;
        case 6: verb = "hit";
        break;
        case 7: verb = "alerted";
        break;  
        case 8: verb = "threatened";
        break;
        case 9: verb = "knocked";
        break;
      }
      return verb;
    }
 
public static String thesis(int a){
  Random random = new Random();
  String results = "A " + Adjective(random.nextInt(10)) + " " + Objective(a) + " " + pastVerb(random.nextInt(10)) + " a " + Adjective(random.nextInt(10))+ " " + Objective(random.nextInt(10)) + " with the " + subNoun(random.nextInt(10))+ ".";
  return results;
}

public static String paragraph(int a){
  Random random = new Random();
  String results1 = "This " + Objective(a) + " then used " + subNoun(random.nextInt(10)) + " to " + pastVerb(random.nextInt(10)) + " the " + Objective(random.nextInt(10)) + ".";
  String results2 = "\nThe " + Objective(a) + " is " + Adjective(random.nextInt(10)) + ".";
  String result = results1 + results2;
  return result; 
  }
}//end of program