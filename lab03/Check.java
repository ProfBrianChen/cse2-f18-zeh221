//////////////
//Zezheng Hua
//Lehigh University
//CSE-002
//zeh221@lehigh.edu
//In this program, i will set up a Scanner class to determine how much each person in the group needs to spend to pay the check
import java.util.Scanner;
public class Check{
  public static void main(String args[]){
  Scanner myScanner = new Scanner( System.in ); //Define the scanner
    System.out.print("Enter the original cost of the check in the form xx.xx: "); //Print the cost out
    
    double checkCost = myScanner.nextDouble(); //Define the value checkCost
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " ); //Print the percentage tip
    
    double tipPercent = myScanner.nextDouble();
    tipPercent /=100; //we want to convert the percentage into a decimal value
    System.out.print("Enter the number of people who went out to dinner: "); //Number of people

    int numPeople = myScanner.nextInt(); //Define number of people
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies; //whole dollar amount of cost
    //for storing digits
    //to the right of the decimal point 
    //for the cost$ 

    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    //get the whole amount, dropping decimal fraction
    dollars=(int)costPerPerson;
    //get dimes amount, e.g., 
    // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
    //  where the % (mod) operator returns the remainder
    //  after the division:   583%100 -> 83, 27%5 -> 2 
    dimes=(int)(costPerPerson * 10) % 10;
    pennies=(int)(costPerPerson * 100) % 10;
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);


  
    
  } //end of the main method 
} //end of class