//////////////
//Zezheng Hua
//Lehigh University
//09/18/2018
//zeh221
//input data and practice arithmetic operations

import java.util.Scanner;//Let the computer know that I am going to use Scanner in this program

public class Convert{ 
  //common start for a java program
  public static void main (String args[]){
  
  //Define variables and constants
  double numberOfAcres;//number of acres of land affected by hurricane
  double raindropAverage;//inches of rain
  double rainAmount;
  final double gallonToCubicmiles = 9.0817e-13;//Convert gallon to cubic miles
  final double gallons = 27154;//the volume of water if there is a one inch water on one acre of land
  double areaInGallon;
  double areaInCubicmiles;
  
  //Define the input
  Scanner scan = new Scanner(System.in);
  
  //Asking for area in acres
  System.out.println("The number of affected area in acres is: ");
  
  //put the input
  numberOfAcres = scan.nextDouble();
    
  //Asking for rainfall
  System.out.println("The rainfall in the affected area is: ");
    
  raindropAverage = scan.nextDouble();
    
  //Convert the units
  areaInGallon = raindropAverage * gallons * numberOfAcres;
  areaInCubicmiles = areaInGallon * gallonToCubicmiles;
    
  //Show the outcome
  System.out.println("The amount of rainfall is " + areaInCubicmiles + " cubic miles");
  
}
 }