//////////////
//Zezheng Hua
//09/18/2018
//Lehigh University
//zeh221
//compute the volume for a pyramid

import java.util.Scanner;//import scanner

public class Pyramid{
  //common start for java
  public static void main(String args[]){
    
    //Define variables and constants
    double lengthOfSide;//The length of the side of the pyramid
    double heightOfPyramid;//The height of pyramid 
    double baseOfPyramid;//The area of the base of the pyramid
    double volumeOfPyramid;//The volume inside the pyramid
    
    //Define scanner
    Scanner scan = new Scanner(System.in);
    
    //Asking for the square side of the pyramid
    System.out.println("The square side of the pyramid is: ");
    
    lengthOfSide = scan.nextDouble();
    
    //Asking for the height of the pyramid
    System.out.println("The height of the pyramid is: ");
    
    heightOfPyramid = scan.nextDouble();
    
    //The equation of the volume
    baseOfPyramid = lengthOfSide * lengthOfSide;
    volumeOfPyramid = baseOfPyramid * heightOfPyramid/3;
    
    //Outcome
    System.out.println("The volume inside the pyramid is: " + volumeOfPyramid);
    
    
    
  }
  
}